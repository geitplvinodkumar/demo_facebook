from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Profile(models.Model):
  user = models.OneToOneField(User, related_name='user', null=True, blank=True)
  username = models.CharField(max_length=30, default='', blank=True)
  email = models.EmailField(max_length=30, unique=True, null=True, blank=True,)
  profile_picture = models.FileField("Image", upload_to='static/documents', default='static/documents/images.jpg')
  background_image = models.FileField("Image", upload_to='static/documents', default='static/documents/image.png')
  date_of_birth = models.DateField(null=True, blank=True)
  mobile_number = models.CharField(max_length=12, blank=True,)
  work = models.CharField(max_length=30, default='', blank=True)
  education = models.CharField(max_length=30, default='', blank=True)
  language = models.CharField(max_length=30, default='', blank=True)
  religion = models.CharField(max_length=30, default='', blank=True)
  gender = models.CharField(max_length=30, default='', blank=True)
  city = models.CharField(max_length=30, default='', blank=True)
  state = models.CharField(max_length=30, default='', blank=True)
  country = models.CharField(max_length=30, default='', blank=True)

  def __unicode__(self):
        return u'Profile of user: %s' % self.user