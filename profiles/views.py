import json
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext, Context, loader 
from django.http import *
from django.template import Context
from django.utils import timezone
from datetime import datetime, timedelta
from profiles.models import Profile
from .forms import *
from django.core.urlresolvers import reverse

# Create your views here.
@csrf_protect
@login_required(login_url='/login/home/')
def show_profile(request):
  return render_to_response('profiles/show_profile.html',{ 'profile': ProfileForm, 'pro': Profile.objects.all() }, 
    context_instance=RequestContext(request))

@login_required(login_url='/login/home/')
def edit_profile(request, profile_id):
  pro = Profile.objects.get(pk=profile_id)
  if request.method == "POST":
    form = ProfileForm(data=request.POST, instance=pro)
    cover_pro_form = CoverProfileForm(data=request.POST, files=request.FILES, instance=pro)
    picture_form = ProfilePictureForm(data=request.POST, files=request.FILES, instance=pro)
    if form.is_valid():
      pro_form = form.save(commit=False)
      pro_form.user = request.user
      pro_form.save()
      return redirect('/show/profile')
  else:
    return render_to_response('profiles/edit_profile.html',{ 'profile': ProfileForm( instance=pro), 'pro': Profile.objects.all() }, 
    context_instance=RequestContext(request))


@login_required(login_url='/login/home/')
def cover_profile(request, profile_id):
  pro = Profile.objects.get(pk=profile_id)
  if request.method == "POST":
    cover_pro_form = CoverProfileForm(data=request.POST, files=request.FILES, instance=pro)
    if cover_pro_form.is_valid():
      cover_pro_form.save()
      return redirect('/show/profile')
  else:
    return render_to_response('profiles/cover_profile.html',{ 'cover_profile': CoverProfileForm, 
      'pro': Profile.objects.all() }, 
    context_instance=RequestContext(request))

@login_required(login_url='/login/home/')
def profile_picture(request, profile_id):
  pro = Profile.objects.get(pk=profile_id)
  if request.method == "POST":
    picture_form = ProfilePictureForm(data=request.POST, files=request.FILES, instance=pro)
    if picture_form.is_valid():
      picture_form.save()
      return redirect('/show/profile')
  else:
    return render_to_response('profiles/profile_picture.html',{ 'profile_picture': ProfilePictureForm, 
      'pro': Profile.objects.all() }, 
    context_instance=RequestContext(request))