from django import forms
from django.contrib.auth.models import User
from profiles.models import Profile

class ProfileForm(forms.ModelForm):
  class Meta:
    model = Profile
    fields = [ 'email','date_of_birth', 'mobile_number', 'work', 'education', 'language', 'religion', 
    'gender', 'city', 'state', 'country']


class CoverProfileForm(forms.ModelForm):
  class Meta:
    model = Profile
    fields = ['background_image']

class ProfilePictureForm(forms.ModelForm):
  class Meta:
    model = Profile
    fields = ['profile_picture']