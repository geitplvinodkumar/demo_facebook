from django import forms
from django.contrib.auth.models import User
from .models import Comment

class CommentForm(forms.ModelForm):
  class Meta:
    model = Comment
    fields = ['text']
    widgets = {
                'text': forms.TextInput(
                    attrs={'id': 'comment_text', 'required': True, 'placeholder': 'Write comment...'}
                ),
            }