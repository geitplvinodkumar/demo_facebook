from django.shortcuts import render, redirect, render_to_response
from login.forms import *
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext, Context, loader 
from django.contrib.auth import authenticate, login, logout
from django.http import *
from django.utils import timezone
from datetime import datetime, timedelta

@csrf_protect
def register(request):
  if request.method == 'POST':
    form = RegistrationForm(data=request.POST)
    profile_form = RegistrationProfileForm(data=request.POST)
    if form.is_valid() and profile_form.is_valid():
      user = User.objects.create_user(
      username=form.cleaned_data['username'],
      first_name=form.cleaned_data['firstname'],
      last_name=form.cleaned_data['lastname'],
      password=form.cleaned_data['password'],
      )
      user.save()
      profile = profile_form.save()
      profile.username = user.username
      profile.save()
      return HttpResponseRedirect('/register/success/')
    else:
      return HttpResponseRedirect('/register/unsuccess/') 
  else:
      form = RegistrationForm()
      return render_to_response('registration/register.html', {'form': form, 'registrationprofile': RegistrationProfileForm}, 
        context_instance=RequestContext(request))

def register_success(request):
    return render_to_response('registration/register_success.html',)

def register_unsuccess(request):
    return render_to_response('registration/register_unsuccess.html',)

@login_required(login_url='/login/')
# def home(request):
#     return render_to_response(
#     'login/home.html',
#     { 'user': request.user }
#     )

def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')  
