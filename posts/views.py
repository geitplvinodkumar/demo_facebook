import json
from django.shortcuts import render, redirect, render_to_response
from posts.forms import *
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext, Context, loader 
from django.http import *
from django.template import Context
from django.utils import timezone
from datetime import datetime, timedelta
from posts.forms import *
from posts.models import Post
from comments.forms import *
from django.core.urlresolvers import reverse
# Create your views here.
@csrf_protect
@login_required(login_url='/login/home/')
def create_post(request):
    user = request.user
    if request.method == "POST":
        form = PostForm(request.POST, request.FILES)
        message = 'something wrong!'
        # import pdb; pdb.set_trace()
        if form.is_valid():
            blogpost = form.save(commit=False)
            blogpost.user = user
            blogpost.name = user.username
            blogpost.save()
            print(request.POST['write_post'])
            message = request.POST['write_post']
            return redirect('/home')

        return HttpResponse(json.dumps({'message': message}))

    return render_to_response('posts/create_post.html',
            {'form':PostForm()}, RequestContext(request))

@login_required(login_url='/login/home/')
def show_post(request):
  two_days_ago = timezone.now() - timedelta(weeks=1)
  recent_posts = Post.objects.filter(upload_date__gt=two_days_ago).all()
  template = loader.get_template('login/home.html')
  context = RequestContext(request, {'post_list': recent_posts, 'comment_list': Comment.objects.all() } )
  return HttpResponse(template.render(context))

@login_required(login_url='/login/home/')
def edit_post(request, slug):
    post = Post.objects.get(slug=slug)
    if request.method == "POST":
      form = EditForm(data=request.POST, files=request.FILES, instance=post)
      # import pdb; pdb.set_trace()
      if form.is_valid():
        # import pdb; pdb.set_trace()
        # form.pic = request.FILES['pic']
        form.save()
        return redirect('/home')
    return render(request, 'posts/edit_post.html', {'edit_post': EditForm, 'p': Post.objects.get(slug=slug)})

@login_required(login_url='/login/home/')
def delete_post(request, id):
  obj = Post.objects.get(pk=id)
  obj.delete()
  return HttpResponseRedirect('/home')