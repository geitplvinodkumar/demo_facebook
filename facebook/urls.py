from django.conf.urls import include, url
from django.contrib.auth import views 
from django.conf import settings
from django.conf.urls.static import static
from django.core.urlresolvers import reverse  
from login.views import *
from profiles.views import *
from posts.views import *
from comments.views import *
from likes.views import *
# from django.contrib import admin

urlpatterns = [
  url(r'^$', views.login, {'template_name': 'login/login.html'}),

  url(r'^register/$', register),

  url(r'^register/success/$', register_success),

  url(r'^register/unsuccess/$', register_unsuccess),

  url(r'^home/$', show_post),

  url(r'^edit/profile/(?P<profile_id>\d+)/$', edit_profile),

  url(r'^cover/profile/(?P<profile_id>\d+)/$', cover_profile),

  url(r'^profile/picture/(?P<profile_id>\d+)/$', profile_picture),

  url(r'^show/profile/$', show_profile),

  url(r'^logout/$', logout_page),

  url(r'^create/post/$', create_post),

  url(r'^edit/post/(?P<slug>[\w-]+)/$', edit_post),
  
  url(r'^delete/post/(?P<id>\d+)/$', delete_post),

  url(r'^create/comment/$', create_comment),

  url(r'^like/$', like),
  # url(r'^comment/detail/(?P<post_id>\d+)/$', comment_detail),

  # url(r'^edit/comment/(?P<comment_id>\d+)/$', edit_comment),

]
